SELECT('SET FOREIGN_KEY_CHECKS=0;');

SELECT concat
        (
            'ALTER TABLE ',
                t1.TABLE_SCHEMA,
                '.',
                t1.table_name,
                ' MODIFY `',
                t1.column_name,
                '` ',
                t1.data_type,
                '(' ,
                    CHARACTER_MAXIMUM_LENGTH,
                ')',
                ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'
        )
from
    information_schema.columns t1
where
t1.TABLE_SCHEMA like 'endi' AND
t1.column_name not in ('login') and
    t1.COLLATION_NAME IS NOT NULL AND
    t1.data_type NOT IN ('mediumtext', 'text', 'longtext', 'enum') AND
    t1.COLLATION_NAME NOT IN ('utf8mb4_unicode_ci');

select 'ALTER table endi.login MODIFY login varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;';
SELECT concat
        (
            'ALTER TABLE ',
                t1.TABLE_SCHEMA,
                '.',
                t1.table_name,
                ' MODIFY `',
                t1.column_name,
                '` ',
                t1.data_type,
                ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'
        )
from
    information_schema.columns t1
where
t1.TABLE_SCHEMA like 'endi' AND
    t1.COLLATION_NAME IS NOT NULL AND
    t1.data_type IN ('mediumtext', 'text', 'longtext') AND
    t1.COLLATION_NAME NOT IN ('utf8mb4_unicode_ci');

select concat (
    'ALTER TABLE ',
    t1.TABLE_SCHEMA,
    '.',
    t1.table_name,
    ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'
)
    from
    information_schema.tables t1
    where
t1.TABLE_SCHEMA like 'endi' AND
t1.TABLE_COLLATION NOT IN ('utf8mb4_unicode_ci');

SELECT('SET FOREIGN_KEY_CHECKS=1;')
