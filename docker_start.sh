#!/bin/bash -e
# ATTENTION : les fichiers docker ne sont actuellement maintenus par personne…

APP_DB_USER=endi
APP_DB_NAME=endi
APP_DB_PASS=endi

cat << EOF | mysql -u root -ppassword --host mysql
      SET GLOBAL max_connect_errors=10000;

      DROP USER IF EXISTS $APP_DB_USER;
      DROP DATABASE  IF EXISTS $APP_DB_USER;
      CREATE DATABASE $APP_DB_NAME;
      CREATE USER '$APP_DB_USER' IDENTIFIED BY '$APP_DB_PASS';
      GRANT ALL PRIVILEGES ON $APP_DB_NAME.* TO '$APP_DB_USER';
      FLUSH PRIVILEGES;
EOF

. /endi/bin/activate
endi-admin development.ini syncdb
pserve development.ini
