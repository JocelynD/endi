import ToolbarAppClass from "./ToolbarAppClass";
import Radio from "backbone.radio";
import ActionToolbar from "../../common/views/ActionToolbar";

const ValidationLimitToolbarAppClass = ToolbarAppClass.extend({
  initialize(){
      this.facade = Radio.channel('facade');
      this.listenTo(this.facade, 'changed:line', this.onLineUpdate);
      this.listenTo(this.facade, 'changed:task', this.onLineUpdate);
      this.listenTo(this.facade, 'changed:discount', this.onLineUpdate);
  },
  onLineUpdate(){
    this.config = Radio.channel('config');
    this.config.request('reload_config').then(()=> {
        const actions = this.config.request('get:form_actions');
        const resume_view = this.getResumeView(actions);
        var view = new ActionToolbar({
            main: actions['main'],
            more: actions['more'],
            resume: resume_view
        });
        this.showView(view);
    })
  },
})

export default ValidationLimitToolbarAppClass;
