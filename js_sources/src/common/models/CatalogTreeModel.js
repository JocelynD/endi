/*
 * File Name :  CatalogTreeModel
 */
import Bb from 'backbone';
import {
    formatAmount
} from '../../math.js';


const CatalogTreeModel = Bb.Model.extend({
    defaults: {
        selected: false,
    },
    matchPattern(search) {
        // i for non-case-sensitive
        const regexp = new RegExp(search, 'i');
        if (
            (this.get('label').search(regexp) !== -1) ||
            (this.get('description').search(regexp) !== -1) ||
            (this.get('category_label').search(regexp) !== -1)
        ) {
            return true;
        }
        return false;
    },
    ht_label(precision = 2) {
        console.log(precision);
        return formatAmount(this.get('ht'), true, false, precision);
    },
    ttc_label(precision = 2) {
        console.log(precision);
        return formatAmount(this.get('ttc'), true, false, precision);
    },
    supplier_ht_label(precision = 2) {
        return formatAmount(this.get('supplier_ht'), true, false, precision);
    }
});
export default CatalogTreeModel;