import Mn from 'backbone.marionette';
import FormBehavior from "../../../base/behaviors/FormBehavior.js";
import CheckboxWidget from '../../../widgets/CheckboxWidget.js';

const DisplayTTCView = Mn.View.extend({
    template: require('./templates/DisplayTTCView.mustache'),
    fields: ['display_ttc'],
    behaviors: [FormBehavior],
    regions: {
        "content": "div",
    },
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    onRender(){
        var value = this.model.get('display_ttc');
        var checked = false;
        if ((value == 1) || (value == '1')) {
            checked = true;
        }
        this.showChildView(
            "content",
            new CheckboxWidget({
                title: "",
                inline_label: "Afficher les prix TTC dans le PDF",
                field_name: "display_ttc",
                checked: checked,
                value: this.model.get('display_ttc'),
            })
        );
    }
});
export default DisplayTTCView;
