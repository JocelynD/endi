import Mn from 'backbone.marionette';
import TaskLineView from './TaskLineView.js';

const TaskLineCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    className: 'lines',
    childView: TaskLineView,
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'line:edit',
    },
    collectionEvents: {
        // Event lancé manuellement depuis le TaskGroupModel
        'updated': 'render'
    },
});
export default TaskLineCollectionView;
