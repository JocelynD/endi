import Mn from 'backbone.marionette';
import TaskGroupView from './TaskGroupView.js';

const TaskGroupCollectionView = Mn.CollectionView.extend({
    tagName: 'div',
    childView: TaskGroupView,
    collectionEvents: {
        'sync': 'render'
    },
    childViewTriggers: {
        'edit': 'group:edit',
    }
});

export default TaskGroupCollectionView;
