import Bb from 'backbone';
import { formatAmount } from '../../math.js';
import Radio from 'backbone.radio';

const TotalModel = Bb.Model.extend({
    isLocalModel: true,  // Avoids to be saved to server
    initialize: function(){
        TotalModel.__super__.initialize.apply(this, arguments);
    },
    getTvaLabel: function(tva_value, tva_key){
        const channel = Radio.channel('config');
        const tva_options = channel.request('get:options', 'tvas');
        let res = {
            'value': formatAmount(tva_value, true),
            'label': 'TVA Inconnue'
        };
        _.each(
            tva_options,
            function(tva){
                if (tva.value == tva_key){
                    res['label'] = tva.name;
                }
            }
        );
        return res
    },
    tva_labels: function(){
        var values = [];
        var this_ = this;
        _.each(this.get('tvas'), function(item, key){
            values.push(this_.getTvaLabel(item, key));
        });
        return values;
    },
    tva_values: function(){
        if (this.has('tvas')){
            // Return tva values used in the form in float format
            const result = Object.keys(this.get('tvas')).map(tva => parseFloat(tva));
            return result;
        } else {
            return {};
        }
        
    }
});
export default TotalModel;
