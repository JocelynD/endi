import Radio from 'backbone.radio';

export const isCancelinvoice = function(){
    const config = Radio.channel('config')
    return config.request('get:form_section', 'composition')['is_cancelinvoice'];
}