import BaseFormWidget from "./BaseFormWidget";
import { getOpt, updateSelectOptions } from "../tools";


const RadioChoiceButtonWidget = BaseFormWidget.extend({
    template: require('./templates/RadioChoiceButtonWidget.mustache'),
    ui: {
        buttons: 'label'
    },
    events: {
        'click @ui.buttons': 'onClick',
    },
    onClick(event){
        let val = $(event.target).val();
        this.triggerFinish(val);
    },
    templateContext: function(){
        var options = this.getOption('options');

        var current_value = this.getOption('value');
        updateSelectOptions(options, current_value);

        let result = this.getCommonContext();
        result['options'] = options;

        return result;
    }
});
export default RadioChoiceButtonWidget;
