/*
 * Module name : ProductResume
 */
import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';
import { round } from "../../../math.js";


const ProductResume = Mn.View.extend({
    template: require('./templates/ProductResume.mustache'),
    modelEvents: {'sync': 'render'},
    initialize(){
        this.user_prefs = Radio.channel('user_preferences');
    },
    templateContext(){
        return {
            supplier_ht_label: this.user_prefs.request('formatAmount', round(this.model.get('flat_cost')), false, false, 5),
            ht_label: this.user_prefs.request('formatAmount', this.model.get('ht'), false),
            total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false)
        };
    }
});
export default ProductResume
