/*
 * Module name : DiscountComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import DiscountCollectionView from './DiscountCollectionView.js';

const template = require('./templates/DiscountComponent.mustache');

const DiscountComponent = Mn.View.extend({
    template: template,
    regions: {
        collection: {el: 'tbody', replaceElement: true},
        addButton: '.add',
        popin: '.popin',
    },
    // Listen to child view events
    childViewEvents: {
        "up": "onUp",
        "down": "onDown",
        "edit": "onEdit",
        'delete': "onDelete",
        'destroy:modal': 'render',
        'cancel:form': 'render',
        'action:clicked': "onActionClicked",
    },
    initialize(){
        this.app = Radio.channel('app');
    },
    onRender(){
        this.showChildView(
            'collection',
            new DiscountCollectionView({collection: this.collection})
        );
    },
    onUp: function(childView){
        console.log("Moving up the element");
        this.collection.moveUp(childView.model);
    },
    onDown: function(childView){
        this.collection.moveDown(childView.model);
    },
    onEdit(childView){
        let route = '/discounts/';
        this.app.trigger('navigate', route + childView.model.get('id'));
    },
    onDelete(childView){
        this.app.trigger('discount:delete', childView);
    }
});
export default DiscountComponent