/* global AppOption; */
import $ from 'jquery';
import _ from 'underscore';

import { applicationStartup } from '../backbone-tools.js';
import App from './components/App.js';
import Facade from './components/Facade.js';
import ToolbarApp from './components/ToolbarApp.js';
import UserPreferences from './components/UserPreferences.js';

$(function(){
    console.log(Facade.radioRequests);
    applicationStartup(AppOption, App, Facade, ToolbarApp, UserPreferences);
});
