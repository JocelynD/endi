import Mn from 'backbone.marionette'
import ConfigBus from 'base/components/ConfigBus';
import Radio from 'backbone.radio'

const StatServiceClass = Mn.Object.extend({
    channelName: 'stat',
    radioRequests: {
        'find:column': 'findModelColumn',
        'find:methods': 'findMethods',
        'get:attributes': '_getAttributes',
        'get:relationships': '_getRelationships',
        'find:static_options': "findStaticOptions",
        "find:manytoone_options": "findManyToOneOptions",
    },
    _getColumnLevel(model_or_path){
        let path = [];
        console.log("Looking for column level informations");
        console.log(model_or_path);
        if (Array.isArray(model_or_path)){
            path = model_or_path;
        } else {
            let model = model_or_path;
            if (model && model.collection && model.collection.path){
                path = model.collection.path;
            }
        }
        
        let result = this.columns;
        // On commence par descendre une éventuelle arborescence d'options
        if (! path){
            path = [];
        } else if (! Array.isArray(path)){
            path = [path];
        }
        for (parent of path){
            result = result.relationships[parent];
        }
        return result;
    },
    _clone(options){
        return JSON.parse(JSON.stringify(options));
    },
    _getAttributes(model_or_path){
        let column_node = this._getColumnLevel(model_or_path);
        return this._clone(column_node.attributes);
    },
    _getRelationships(model_or_path){
        let column_node = this._getColumnLevel(model_or_path);
        return this._clone(column_node.relationships);
    },
    findModelColumn(key, path=[]){
        /*
         * Find the Column of the model
         */
        console.log("Find the model for a given column %s %s", key, path)
        
        let parent_node = this._getColumnLevel(path);
        // On récupère le résultat
        let result;
        if (key in parent_node.attributes ){
            result = parent_node.attributes[key];
        } else if (parent_node.relationships && (key in parent_node.relationships)){
            result = parent_node.relationships[key];
        } else {
            console.error("Erreur, il y a des anciennes options statistiques qui ne sont plus supportées %s", key);
        }
        console.log(result);
        return this._clone(result);
    },
    findMethods(type){
        return this._clone(this.form_config.options.methods[type]);
    },
    findStaticOptions(key){
        return this._clone(this.form_config.options.static_opt_options[key]);
    },
    findManyToOneOptions(key){
        return this._clone(this.form_config.options.manytoone_options[key]);
    },
    setFormConfig(form_config) {
        this.form_config = form_config;
        this.columns = form_config.options.columns;
    },
    setup(form_config_url){
    },
    start(){
        this.setFormConfig(ConfigBus.form_config);
        let result = $.Deferred();
        result.resolve(ConfigBus.form_config, null, null);
        return result;
    }
})
const StatService = new StatServiceClass();
export default StatService;