import pytest

from endi.compute.sale_product.ht_mode import SaleProductHtComputer
from endi.compute.sale_product.ttc_mode import SaleProductTtcComputer

from endi.compute.sale_product.supplier_ht_mode import SaleProductSupplierHtComputer


class TestHtMode:
    @pytest.fixture
    def computer(self, mk_sale_product, tva20):
        p = mk_sale_product(
            supplier_ht=5000000,
            ht=1000000,
            mode="ht",
            tva=tva20,
        )
        return SaleProductHtComputer(p)

    def test_unit_ht(self, computer):
        assert computer.unit_ht() == 1000000

    def test_unit_ttc(self, computer):
        assert computer.unit_ttc() == 1200000


class TestTtcMode:
    @pytest.fixture
    def computer(self, mk_sale_product, tva20):
        p = mk_sale_product(
            supplier_ht=5000000,
            ttc=1000000,
            mode="ttc",
            tva=tva20,
        )
        return SaleProductTtcComputer(p)

    def test_unit_ht(self, computer):
        assert int(computer.unit_ht()) == 833330

    def test_unit_ttc(self, computer):
        assert computer.unit_ttc() == 1000000


class TestSupplierHtMode:
    @pytest.fixture
    def computer(self, mk_sale_product, tva20):
        p = mk_sale_product(
            supplier_ht=1000000,
            general_overhead=0.11,
            margin_rate=0.12,
            tva=tva20,
            mode="supplier_ht",
        )
        # assert int(base_product.ht) == 126136
        return SaleProductSupplierHtComputer(p)

    def test_flat_cost(self, computer):
        assert computer.flat_cost() == 1000000
        computer.sale_product.supplier_ht = None
        assert computer.flat_cost() == 0

    def test_cost_price(self, computer):
        assert computer.cost_price() == 1110000
        computer.sale_product.general_overhead = 0
        assert computer.cost_price() == 1000000

    def test_intermediate_price(self, computer):
        assert int(computer.intermediate_price()) == 1261363
        computer.sale_product.margin_rate = None
        assert int(computer.intermediate_price()) == 1110000

    def test_unit_ht(self, computer, company):
        assert int(computer.unit_ht()) == 1261363

        # Avec contribution :
        company.contribution = 10
        assert int(computer.unit_ht()) == 1401515

    def test_unit_ttc(self, computer):
        assert int(computer.unit_ttc()) == 1513636
