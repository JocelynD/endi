import pytest
import datetime
from endi.models.task.services import (
    TaskService,
)


from endi.models.task import Task


@pytest.fixture
def task_insurance_option(mk_task_insurance_option):
    return mk_task_insurance_option(rate=6)


@pytest.fixture
def task(
    dbsession,
    tva,
    product,
    customer,
    project,
    user,
    company,
    phase,
    mk_task_line,
):
    result = Task(
        project=project,
        customer=customer,
        company=company,
        user=user,
        phase=phase,
        start_date=datetime.date(2021, 2, 1),
        first_visit=datetime.date(2021, 1, 20),
        end_date="Deux semaines après le début",
    )
    result.line_groups[0].lines.append(mk_task_line(group=result.line_groups[0]))
    dbsession.add(result)
    dbsession.flush()
    return result


@pytest.fixture
def valid_status_log_entry(mk_status_log_entry, task, user):
    return mk_status_log_entry(
        node_id=task.id,
        status="valid",
        user_id=user.id,
        state_manager_key="status",
    )


@pytest.fixture
def draft_status_log_entry(mk_status_log_entry, task, user2):
    return mk_status_log_entry(
        node_id=task.id,
        status="draft",
        user_id=user2.id,
        state_manager_key="status",
    )


class TestTaskService:
    def test_duplicate(self, task, user, project, customer, phase):
        result = TaskService.duplicate(
            task,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        for field in (
            "description",
            "mode",
            "display_units",
            "display_ttc",
            "expenses_ht",
            "workplace",
            "payment_conditions",
            "notes",
            "company",
            "customer",
            "project",
            "phase",
            "address",
            "insurance_id",
            "end_date",
        ):
            assert getattr(result, field) == getattr(task, field)
        assert result.status == "draft"
        assert result.owner == user
        assert result.status_person == user

    def test_duplicate_mode(self, task, user, project, customer, phase):
        task.mode = "ttc"
        result = TaskService.duplicate(
            task,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert result.mode == "ttc"
        task.mode = "ht"
        result = TaskService.duplicate(
            task,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert result.mode == "ht"

    def test_duplicate_decimal_to_display(
        self,
        task,
        user,
        project,
        customer,
        phase,
        company,
    ):
        result = TaskService.duplicate(
            task,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert result.decimal_to_display == 2
        company.decimal_to_display = 5
        result = TaskService.duplicate(
            task,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert result.decimal_to_display == 5

    def test_filter_by_validator_id_no_status(self, task, user, user2):
        assert TaskService.query_by_validator_id(Task, user2.id).count() == 0
        assert TaskService.query_by_validator_id(Task, user.id).count() == 0

    def test_filter_by_validator_id(
        self,
        task,
        draft_status_log_entry,
        valid_status_log_entry,
    ):
        validator_id = valid_status_log_entry.user_id
        draftor_id = draft_status_log_entry.user_id
        assert TaskService.query_by_validator_id(Task, draftor_id).count() == 0
        assert TaskService.query_by_validator_id(Task, validator_id).count() == 1
        assert TaskService.query_by_validator_id(Task, validator_id).first() == task

    def test_filter_by_validator_id_w_custom_query(
        self,
        task,
        valid_status_log_entry,
    ):
        validator_id = valid_status_log_entry.user_id
        query = Task.query().filter_by(official_number="DONOTEXIST")
        assert (
            TaskService.query_by_validator_id(Task, validator_id, query=query).count()
            == 0
        )

    def test_get_rate(
        self,
        dbsession,
        mk_custom_invoice_book_entry_module,
        task_insurance_option,
        task,
    ):
        mk_custom_invoice_book_entry_module(
            name="contribution", percentage=10, doctype="invoice"
        )
        mk_custom_invoice_book_entry_module(
            name="contribution", percentage=10, doctype="internalinvoice"
        )
        task.company.contribution = 5.25
        dbsession.merge(task.company)
        dbsession.flush()
        assert TaskService.get_rate(task, "contribution") == 5.25
        task.prefix = "internal"
        assert TaskService.get_rate(task, "contribution") == 10
        task.company.internalcontribution = 7.2
        dbsession.merge(task.company)
        dbsession.flush()
        assert TaskService.get_rate(task, "contribution") == 7.2
        task.prefix = ""
        assert TaskService.get_rate(task, "insurance") is None

        mk_custom_invoice_book_entry_module(
            name="insurance", percentage=10, doctype="invoice"
        )
        assert TaskService.get_rate(task, "insurance") == 10
        task.company.insurance = 8.1
        dbsession.merge(task.company)
        dbsession.flush()
        assert TaskService.get_rate(task, "insurance") == 8.1
        task.insurance = task_insurance_option
        dbsession.merge(task)
        dbsession.flush()
        assert TaskService.get_rate(task, "insurance") == 6
