import colander
import deform
import colanderalchemy
import functools

from endi import forms
from endi.forms.lists import BaseListsSchema
from endi.models.price_study.price_study import PriceStudy
from endi.models.project.project import Project
from endi.models.project.types import ProjectType


def customize_schema(schema):
    customize = functools.partial(forms.customize_field, schema)
    customize("name", title="Nom interne de l’étude de prix")
    customize(
        "notes",
        title="Notes internes sur cette étude de prix",
        widget=deform.widget.TextAreaWidget(),
    )
    return schema


def get_price_study_add_edit_schema():
    schema = colanderalchemy.SQLAlchemySchemaNode(
        PriceStudy,
        includes=(
            "name",
            "notes",
        ),
    )
    schema = customize_schema(schema)
    return schema


def get_list_schema():
    schema = BaseListsSchema().clone()

    schema["search"].title = "Nom de l’étude de prix"

    schema.add(
        colander.SchemaNode(
            colander.Boolean(),
            name="with_estimation",
            title="",
            label="Uniquement les études de prix associées à un devis",
            missing=False,
        )
    )
    return schema


@colander.deferred
def deferred_project_widget(node, kw):
    cid = kw["request"].context.company_id
    projects = Project.query_for_select(cid)
    project_type_ids = [
        t.id for t in ProjectType.query().filter_by(include_price_study=True)
    ]
    projects = projects.filter(Project.project_type_id.in_(project_type_ids))
    return deform.widget.SelectWidget(values=projects)


@colander.deferred
def deferred_default_project(node, kw):
    return kw["request"].context.project_id


class DuplicateSchema(colander.Schema):
    """
    schema used to initialize a new task
    """

    project_id = colander.SchemaNode(
        colander.Integer(),
        title="Dossier dans lequel dupliquer l'étude",
        widget=deferred_project_widget,
        default=deferred_default_project,
    )


def get_duplicate_schema():
    return DuplicateSchema()
