import colander
from endi.forms import files
from endi import forms


class SiteConfigSchema(colander.MappingSchema):
    """
    Site configuration
    logos ...
    """

    logo = files.ImageNode(
        title="Choisir un logo",
        missing=colander.drop,
        description="Charger un fichier de type image *.png *.jpeg \
 *.jpg…",
    )

    welcome = forms.textarea_node(
        title="Texte d'accueil",
        richwidget=True,
        missing="",
        admin=True,
    )
