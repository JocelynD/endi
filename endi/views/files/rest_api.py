from endi.models.files import File
from endi.views import BaseRestView


class FileRestView(BaseRestView):
    """
    For files with a parent model (attachments).

    Should be implemented on a route with a context. This context will be the
    parent of file.

    Does not handle file category
    """

    def post_format(self, entry: File, edit: bool, attributes):
        if edit:
            raise NotImplementedError
        else:
            entry.parent = self.context
            return entry

    def query(self):
        return File.query()
