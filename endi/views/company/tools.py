import os
from .routes import ITEM_ROUTE


def get_company_url(request, company=None, subpath=None, **kwargs):
    """
    Build an url to access company views
    """
    url = ITEM_ROUTE

    if subpath:
        if subpath.startswith("/"):
            subpath = subpath[1:]
        url = os.path.join(url, subpath)
    if company:
        company_id = company.id
    else:
        company_id = request.context.id
    return request.route_path(url, id=company_id, _query=kwargs)
