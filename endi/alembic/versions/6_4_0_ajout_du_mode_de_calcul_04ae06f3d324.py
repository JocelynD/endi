"""6.4.0 Ajout du mode de calcul au catalogue

Revision ID: 04ae06f3d324
Revises: 48cbb45ca42d
Create Date: 2021-11-30 17:35:50.519552

"""

# revision identifiers, used by Alembic.
revision = "04ae06f3d324"
down_revision = "5d2700d4a141"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def update_database_structure():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "base_sale_product", sa.Column("mode", sa.String(length=20), nullable=False)
    )
    op.add_column("base_sale_product", sa.Column("ttc", sa.BigInteger(), nullable=True))
    # ### end Alembic commands ###


def migrate_datas():
    from alembic.context import get_bind
    from zope.sqlalchemy import mark_changed
    from endi_base.models.base import DBSESSION

    session = DBSESSION()
    conn = get_bind()
    session.execute("update base_sale_product set mode='ht';")
    session.execute(
        "update base_sale_product set mode='supplier_ht' where supplier_ht is not null"
        " and supplier_ht > 0;"
    )

    from endi.models.sale_product.base import BaseSaleProduct

    for p in BaseSaleProduct.query():
        p.sync_amounts()

    mark_changed(session)
    session.flush()


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("base_sale_product", "ttc")
    op.drop_column("base_sale_product", "mode")
