from typing import Iterable


def fix_sage_ordering(items: Iterable[dict]) -> Iterable[dict]:
    """
    Ensure the line ordering of an export suits sage.

    For that, make sure each G-line is followed by all its matching A-line(s).

    Requirements:
    - Each A-line is anotated with a '_general_counterpart' key containing its general row.
    - if two A-line got groupped, their G-lines should have been grouped together also

    Considerations:
    - slicing/insertion with large python lists costs O(n), so we try avoiding it.

    The algorithm consists into two passes, one to detect orphans, and one to
    all items (incl. orphans) in the right order.
    """
    # First pass (no not defer yielding to be sure to have collected all orphans)
    anotated = list(_track_analytical_orphans(items))

    # Second pass
    for i in _with_orphans_inplace(anotated):
        yield i


def _track_analytical_orphans(items: Iterable[dict]) -> Iterable[dict]:
    """
    - For well-placed lines, will yield them as they come
    - For orphaned analytical lines, will list them in a list under the
      '_analytic_orphans' key on their reference row.
    """

    last_yielded_g = None

    for item in items:
        if item["type_"] == "A":
            g_counterpart = item["_general_counterpart"]
            if id(last_yielded_g) == id(g_counterpart):
                # A-line is following its G-line, everything OK for sage
                yield item
            else:
                try:
                    g_counterpart["_analytic_orphans"].append(item)
                except KeyError:
                    g_counterpart["_analytic_orphans"] = [item]
        else:  # G
            yield item
            last_yielded_g = item


def _with_orphans_inplace(items: Iterable[dict]) -> Iterable[dict]:
    """
    Flatten a list containing nested orphans

    Input format is data as yielded by _track_analytical_orphans
    """
    queued_orphans = []
    for item in items:
        if item["type_"] == "G":
            # Flush orphans before issuing a new G-line
            # not before, to preserve ordering as much as possible.
            for orphan_item in queued_orphans:
                yield orphan_item
            yield item
            queued_orphans = item.pop("_analytic_orphans", [])
        else:  # A
            yield item
    for orphan_item in queued_orphans:
        yield orphan_item
