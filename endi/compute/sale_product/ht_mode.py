from endi.compute.math_utils import compute_tva


class SaleProductHtComputer:
    def __init__(self, sale_product):
        self.sale_product = sale_product

    def unit_ht(self):
        return self.sale_product.ht

    def unit_ttc(self):
        """
        Compute the ttc value for the given sale product
        """
        ht = self.unit_ht()
        tva = self.sale_product.tva
        if tva is not None:
            return ht + compute_tva(ht, tva.value)
        else:
            return ht
