<div class="dash_elem">
    <h2>
        <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg></span>
        <span>${title}</span>
    </h2>
    <div class='panel-body'>
        <table class="hover_table">
            <caption class="screen-reader-text">Liste des ${title}</caption>
            <thead>
                <tr>
                    <th scope="col" class='col_text'>Nom</th>
                    <th scope="col" class='col_text'>Enseigne</th>
                    <th scope="col" class='col_date'>Demandé le</th>
                </tr>
            </thead>
            <tbody>
                % for task in dataset:
                    <tr class="clickable-row" data-href="${task.url}" title="${file_hint}&nbsp;: ${task.name}">
                        <td class='col_text'><a href="${task.url}">${task.name}</a></td>
                        <td class='col_text'>${task.get_company().name}</td>
                        <td class='col_date'>${api.format_date(task.status_date)}</td>
                    </tr>
                % endfor
                % if not dataset:
                    <tr><td class="col_text" colspan='3'>Aucun document en attente</td></tr>
                % endif
            </tbody>
        </table>
    </div>
</div>
