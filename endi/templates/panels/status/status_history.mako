% for status in entries:
	% if loop.first:
		<div class="status_history hidden-print">
			<h4 title="Historique des commentaires et changements de statut"><span class="screen-reader-text">Historique des </span>Commentaires et changements de statut</h4>
			<ul>
	% endif
    % if not (status.status == 'unknown' and not status.comment):
	    <li>
	    	% if api.status_css_class(status):
		    <blockquote class="${api.status_css_class(status)}">
		    % else:
		    <blockquote>
		    % endif
			    % if status.status and status.status != 'unknown':
	                <span class="icon status ${api.status_css_class(status)}" role="presentation">
                        ${api.icon(api.status_icon(status))}
	                </span>
				    <p class="status">

				    ${api.format_status_string(status, genre)}
				    <p>
			    % endif
				    % if status.comment:
				    <p>
				    ${status.comment | n}
				    <p>
				    % endif
				    <footer>
					    ${api.format_account(status.user)} le ${api.format_date(status.datetime)}
				    </footer>
		    </blockquote>
	    </li>
    % endif
	% if loop.last:
			</ul>
		</div>
	% endif
% endfor
