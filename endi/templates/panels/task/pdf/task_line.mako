<%doc>
Renders a TaskLine object
</%doc>

<%namespace file="/base/utils.mako" import="format_text" />

% for description_line in description_lines:
% if loop.first:
<tr>
    % if display_date_column:
        <td class="col_date date">${api.format_date(line.date)}</td>
    % endif
    <td class="col_text description rich_text">${format_text(description_line, False)}</td>
    % if display_units == 1:
        <%doc>
        We display the unit ht value if :
        - we're in ht mode
        - we're in ttc mode with display_ttc set to False
        </%doc>
        <td class="col_number price">${task.format_amount(unit_ht, trim=False, precision=5)}&nbsp;€</td>
        <td class="col_number quantity">${api.format_quantity(quantity)}</td>
        <td class="col_text unity">${unity}</td>
    % endif
    % if show_progress_invoicing:
        % if show_previous_invoice:
        <td class="col_number progress_invoicing archive">${progress_invoicing_invoiced_percentage}&nbsp;%</td>
        % endif
        <td class="col_number progress_invoicing">${progress_invoicing_percentage}&nbsp;%</td>
    % endif
    <td class="col_number price_total">
        % if is_tva_on_margin_mode:
            ${task.format_amount(total, trim=False, precision=5)}&nbsp;€
        % else:
            ${task.format_amount(total_ht, trim=False, precision=5)}&nbsp;€
        % endif
    </td>
    % if display_tvas_column and not is_tva_on_margin_mode:
        <td class="col_number tva">
            % if line.tva>=0:
                ${task.format_amount(line.tva, precision=2)}&nbsp;%
            % else:
                0 %
            % endif
        </td>
    % endif
    % if display_ttc:
        <td class="col_number price">${task.format_amount(total, trim=False, precision=5)}&nbsp;€</td>
    % endif
</tr>
% else:
<tr class='long_description'>
    % if display_date_column:
        <td class="col_date date"></td>
    % endif
    <td class="col_text description rich_text">${format_text(description_line, False)}</td>
    % if display_units == 1:
    <td class="col_number price"></td>
    <td class="col_number quantity"></td>
    <td class="col_text unity"></td>
    % endif
    % if show_progress_invoicing:
        % if show_previous_invoice:
        <td class="col_number progress_invoicing archive"></td>
        % endif
        <td class="col_number progress_invoicing"></td>
    % endif
    <td class="col_number price_total"></td>
    % if display_tvas_column and not is_tva_on_margin_mode:
        <td class="col_number tva"></td>
    % endif
    % if display_ttc:
        <td class="col_number price"></td>
    % endif
</tr>
% endif 
% endfor