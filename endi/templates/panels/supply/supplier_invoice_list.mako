<%namespace file="/base/pager.mako" import="sortable"/>

% if records:
<table class="hover_table">
    <thead>
        <tr>
			<th scope="col" class="col_status col_text"></th>
            <th scope="col" class="col_text">${sortable("N° de pièce", "official_number")}</th>
            % if is_admin_view:
                <th scope="col" class="col_text">${sortable("Enseigne", "company_id")}</th>
            % endif
            <th scope="col" class="col_date">${sortable("Date", "date")}</th>
            <th scope="col" class="col_text">
                ${sortable("N° de facture du fournisseur", "remote_invoice_number")}
            </th>
            % if not is_supplier_view:
                <th scope="col" class="col_text">${sortable("Fournisseur", "supplier")}</th>
            % endif
            <th scope="col" class="col_number">HT</th>
            <th scope="col" class="col_number">TVA</th>
            <th scope="col" class="col_number">TTC</th>
            <th scope="col" class="col_number" title="Part réglée par la CAE">Part CAE</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </tr>
    </thead>
    <tbody>
% else:
<table>
    <tbody>
		<tr>
			<td class='col_text'>
				<em>
				% if is_search_filter_active:
					Aucune facture fournisseur correspondant à ces critères.
				% else:
					Aucune facture fournisseur pour linstant.
				% endif
				</em>
			</td>
		</tr>
% endif
	% for supplier_invoice in records:
		<tr class='tableelement' id="${supplier_invoice.id}">
			<% url = request.route_path("/suppliers_invoices/{id}", id=supplier_invoice.id) %>
			<% onclick = "document.location='{url}'".format(url=url) %>
			<% tooltip_title = "Cliquer pour voir ou modifier la facture « " + supplier_invoice.remote_invoice_number + " »" %>
			<td class="col_status" onclick="${onclick}" title="${api.format_status(supplier_invoice)} - ${tooltip_title}">
				<span class="icon status ${supplier_invoice.global_status}">${api.icon(api.status_icon(supplier_invoice))}</span>
			</td>
			<td class="col_text" onclick="${onclick}" title="${tooltip_title}">${supplier_invoice.official_number}</td>
			% if is_admin_view:
				<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
					${supplier_invoice.company.name}
				</td>
			% endif
			<td class="col_date" onclick="${onclick}" title="${tooltip_title}">${api.format_date(supplier_invoice.date)}</td>
			<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
				${supplier_invoice.remote_invoice_number}
<!-- Si admin et facture auto-validée
				<br>
				<span class="icon tag positive">                        		
					${api.icon("user-check")}
					Auto-validé
				</span>
-->
			</td>
			% if not is_supplier_view:
			<td class="col_text" onclick="${onclick}" title="${tooltip_title}">${supplier_invoice.supplier_label}</td>
			% endif
			<td class="col_number" onclick="${onclick}" title="${tooltip_title}">${api.format_amount(supplier_invoice.total_ht)}</td>
			<td class="col_number" onclick="${onclick}" title="${tooltip_title}">${api.format_amount(supplier_invoice.total_tva)}</td>
			<td class="col_number" onclick="${onclick}" title="${tooltip_title}">${api.format_amount(supplier_invoice.total)}</td>
			<td class="col_number" onclick="${onclick}" title="${tooltip_title}">${supplier_invoice.cae_percentage} %</td>
			${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(supplier_invoice))}
		</tr>
	% endfor
	</tbody>
</table>
