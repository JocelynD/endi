<%namespace file="/base/utils.mako" name="utils" />
<tr>
	<td class="col_status" title="${indicator.status_label}" aria-label="${indicator.status_label}">
		<span class='icon status ${indicator.status}'>
			${api.icon(status_icon)}
		</span>
	</td>
	<td class="col_icon col_status" title="Cet indicateur est requis pour la facture" aria-label="Cet indicateur est requis pour la facture">
        <!-- Icône du niveau auquel l’indicateur est requis : affaire (#list-alt), dossier (#folder), devis (#file-alt), facture (#file-invoice-euro)
          Pour le moment, tous les documents requis le sont pour la facturation.
        -->
		<span class="icon">
			${api.icon('file-invoice-euro')}
		</span>
	</td>
	<td class="col_text">
		${indicator.file_type.label}
	</td>
	<td class="col_text">
		% if indicator.file_id:
			<a href="#" onclick="openPopup('${request.route_path('/files/{id}', id=indicator.file_object.id)}')" title="Ouvrir le fichier dans une nouvelle fenêtre" aria-label="Ouvrir le fichier dans une nouvelle fenêtre">
				${indicator.file_object.name} (${api.human_readable_filesize(indicator.file_object.size)})
			</a>
			% if request.has_permission('valid.indicator', indicator):
				% if not indicator.validation_status == 'valid':
				<em>Fichier en attente de validation</em>
				% endif
			% endif
		% elif indicator.forced:
			<em>Cet indicateur a été forcé manuellement</em>
		% else:
			<em>Aucun fichier fourni</em>
		% endif
	</td>
	<td class="col_actions width_two">
		% if indicator.file_id:
			% if request.has_permission('valid.indicator', indicator):
				% if not indicator.validation_status == 'valid':
				<% validate_file_url =  request.route_path(force_route, id=indicator.id, _query={'action': 'validation_status', 'validation_status': 'valid'}) %>
				<%utils:post_action_btn url="${validate_file_url}" icon="check"
				  _class='btn icon only'
				  title="Valider le fichier fourni"
				  aria_label="Valider le fichier fourni"
				>
				</%utils:post_action_btn>
				% else:
				<% validate_file_url =  request.route_path(force_route, id=indicator.id, _query={'action': 'validation_status', 'validation_status': 'invalid'}) %>
				<%utils:post_action_btn url="${validate_file_url}" icon="times"
				  _class='btn icon only negative'
				  title="Invalider le fichier fourni"
				  aria_label="Invalider le fichier fourni"
				>
				</%utils:post_action_btn>
				% endif
			% endif
		% endif
		% if request.has_permission('add.file', indicator):
			<button
				class='btn icon only'
				onclick="window.openPopup('${file_add_url}?file_type_id=${indicator.file_type_id}')"
				title="Ajouter un fichier (s’ouvrira dans une nouvelle fenêtre)"
				aria-label="Ajouter un fichier (s’ouvrira dans une nouvelle fenêtre)"
				>
				${api.icon('paperclip')}
				Ajouter un fichier
			</button>
		% endif
		% if request.has_permission('force.indicator', indicator):
			<% force_indicator_url = request.route_path(force_route, id=indicator.id, _query={'action': 'force'}) %>
			% if not indicator.forced:
				<%utils:post_action_btn url="${force_indicator_url}" icon="bolt"
					_class="btn icon only negative"
					onclick="return confirm('Êtes-vous sûr de vouloir forcer cet indicateur (il apparaîtra désormais comme valide) ?');"
					title="Forcer cet indicateur"
					aria_label="Forcer cet indicateur"
				>
				</%utils:post_action_btn>
			% else:
				<%utils:post_action_btn url="${force_indicator_url}" icon="undo-alt"
					_class='btn icon only negative'
					title="Invalider cet indicateur"
					aria_label="Invalider cet indicateur"
				>
				</%utils:post_action_btn>
				% endif
		% endif
	</td>
</tr>
