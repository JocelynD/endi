<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <div role='group'>
        % if api.has_permission('add_customer'):
            <button class='btn btn-primary icon' onclick="toggleModal('customer_add_form'); return false;">
                ${api.icon("plus")}Ajouter<span class="no_mobile">&nbsp;un client</span>
            </button>
            <a class='btn icon' href="${request.route_path('company_customers_import_step1', id=request.context.id)}">
                ${api.icon("file-import")}Importer<span class="no_mobile">&nbsp;des clients</span>
            </a>
        % endif
    </div>
    <%
    ## We build the link with the current search arguments
    args = request.GET
    url = request.route_path('customers.csv', id=request.context.id, _query=args)
    %>
    <a class='btn icon_only_mobile' href='${url}' title="Export au format CSV" >
        ${api.icon("file-csv")}CSV
    </a>
</div>
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
    	% if records:
        <table class="hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
                    <th scope="col">${sortable("Code", "code")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du client", "label")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du contact principal", "lastname")}</th>
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
        % else:
        <table>
        	<tbody>
				<tr>
					<td colspan='5' class="col_text">
						<em>Aucun client n’a été référencé</em>
					</td>
				</tr>
		% endif
			% for customer in records:
				<tr class='tableelement' id="${customer.id}">
					<% url = request.route_path("customer", id=customer.id) %>
					<% onclick = "document.location='{url}'".format(url=url) %>
					<% tooltip_title = "Cliquer pour voir ou modifier le client « " + customer.label + " »" %>
					<td class="col_date"  onclick="${onclick}" title="${tooltip_title}">${api.format_date(customer.created_at)}</td>
					<td onclick="${onclick}" title="${tooltip_title}">${customer.code}</td>
					<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
						% if customer.archived:
							<small title="Ce client a été archivé"><span class='icon'>${api.icon("archive")} Client archivé</span></small><br />
						% endif
						${customer.label}
						% if customer.is_internal():
						&nbsp;(interne à la CAE)
						% endif
					</td>
					<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
						${customer.get_name()}
					</td>
					${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(customer))}
				</tr>
			% endfor
            </tbody>
        </table>
	</div>
	${pager(records)}
</div>

<section id="customer_add_form" class="modal_view size_middle" style="display: none;">
    <div role="dialog" id="customer-forms" aria-modal="true" aria-labelledby="customer-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('customer_add_form'); return false;">
                    ${api.icon("times")}
                </button>
                <h2 id="customer-forms_title">Ajouter un client</h2>
            </header>
            <nav>
                <ul class="nav nav-tabs modal-tabs" role="tablist" aria-label="Type de client">
                % for form_key, form_info in forms.items():
                    <li role="presentation"
                        % if form_info.get('selected', 'false') == 'true':
                        class="active"
                        % endif
                        >
                        <a href="#${form_key}Form"
                            aria-controls="${form_key}Form"
                            role="tab"
                            aria-selected="${form_info.get('selected', 'false')}"
                            % if form_info.get('selected', 'false') == 'false':
                            tabindex="-1"
                            % endif
                            >
                            ${form_info['link']}
                            </a>
                    </li>
                    % endfor
                </ul>
            </nav>
            <main>
                <div class="tab-content">
                % for form_key, form_info in forms.items():
                    <div
                    role="tabpanel"
                    class="tab-pane
                    % if form_info.get('selected', 'false') == 'true':
                    active
                    % endif
                    row" id="${form_key}Form" aria-labelledby="${form_key}" tabindex="0">
                        <h3>${form_info['title']|n}</h3>
                        ${form_info['form'].render()|n}
                    </div>
                    % endfor
                </div>
            </main>
        </div>
    </div>
</section>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
