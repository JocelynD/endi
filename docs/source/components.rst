Components
==========

Web server
----------

enDI's web server is constructed with :

* Python
* Pyramid

Database
--------

enDI uses a Mysql database.

To access it, we use the following libraries:

* SQLAlchemy : ORM for database object representation
* Alembic : Database migration (downgrade/upgrade) (cf :doc:`alembic_migrations`)
* Python MysqlDB

Forms handling
--------------

* Deform : form library
* Deform bootstrap : css/javascript Twitter bootstrap extension of deform
* Colander : form validation tool

Javascript
----------

The following js libraries are used:

* Jquery : DOM manipulation
* Jquery Ui : Form elements (datepicker, ...)
* Bootstrap : Fancy effects (accordion, scrolling menu)
* Underscore/backbone/backbone-marionette : MVP datas handling (Model View
  Presentation)
* Handlebars.js : Javascript templating

Css Stylesheet
--------------

Custom stylesheets are used for the layout. Check the Styleguide for more information.
